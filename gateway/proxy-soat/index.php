<?php

require_once __DIR__ . '/../../vendor/autoload.php';

use Chadicus\Slim\OAuth2\Routes;
use Chadicus\Slim\OAuth2\Middleware; 
use Chadicus\Slim\OAuth2\Connection;
use Slim\Slim;
use OAuth2\Server;
use OAuth2\Storage;
use OAuth2\GrantType;

$mongoDb = (new MongoClient())->selectDb('slim_oauth2');

$storage = new Storage\Mongo($mongoDb);
$storage->setClientDetails('isoat', '!QAZxsw2', '/receive-code', null, 'carSearch');
$storage->setClientDetails('nrunt', 'qaws12pj');

$server = new Server(
    $storage,
    [
        'access_lifetime' => 3600,
    ],
    [
        new GrantType\ClientCredentials($storage),
        new GrantType\AuthorizationCode($storage),
    ]
);

$app = new Slim();

Routes\Token::register($app, $server);
Routes\Authorize::register($app, $server);
Routes\ReceiveCode::register($app);

$app->config('templates.path', __DIR__ . '/../../vendor/chadicus/slim-oauth2-routes/templates');

$authorization = new Middleware\Authorization($server);
$authorization->setApplication($app);


$requiresHttps = function () use ($app) {
    if ($app->environment['slim.url_scheme'] !== 'https' ) {
        //$app->redirect('/requiressl');    // or render response and 
        $result = ['error' => 'security error, SSL only'];
        $app->contentType('application/json');
        $app->response->setBody(json_encode($result));
        $app->stop();
     }
};


$app->get('/proxy-runt/consulta/vehiculos', $requiresHttps, $authorization, function () use ($app, $mongoDb) {
    $result = [];
    try {
        $limit = isset($_GET['limit']) ? (int)$_GET['limit'] : 5;
        $offset = isset($_GET['offset']) ? (int)$_GET['offset'] : 0;
        $vehiculos = $mongoDb->runt->find([])->skip($offset)->limit($limit);

        $result = [
            'offset' => $offset,
            'limit' => $vehiculos->count(true),
            'total' => $vehiculos->count(),
            'vehicles' => [],
        ];

        foreach ($vehiculos as $vehiculo) {
            $result['vehicles'][] = [
                'identificador_linea'=>(string)$vehiculo['identificador_linea'],

                'linea'=>$vehiculo['linea'],
                'identificador_marca'=>$vehiculo['identificador_marca'],
                'marca'=>$vehiculo['marca'],
                'modelo'=>$vehiculo['modelo'],
                'numero_de_chasis'=>$vehiculo['numero_de_chasis'],
                'numero_ejes'=>$vehiculo['numero_ejes'],
                'numero_licencia_transito'=>$vehiculo['numero_licencia_transito'],
                'numero_motor'=>$vehiculo['numero_motor'],
                'numero_placa'=>$vehiculo['numero_placa'],
                'numero_registro'=>$vehiculo['numero_registro'],
                'numero_serie'=>$vehiculo['numero_serie'],
                'numero_vin'=>$vehiculo['numero_vin'],
                'capacidad_carga'=>$vehiculo['capacidad_carga'],
                'cilindraje'=>$vehiculo['cilindraje'],
                'identificador_clase_vehiculo'=>$vehiculo['identificador_clase_vehiculo'],
                'clase_vehiculo'=>$vehiculo['clase_vehiculo'],
                'clasificacion'=>$vehiculo['clasificacion'],
                'identificador_color'=>$vehiculo['identificador_color'],
                'color'=>$vehiculo['color'],
                'estado_del_vehiculo'=>$vehiculo['estado_del_vehiculo'],
                'numero_regrabacion_chasis'=>$vehiculo['numero_regrabacion_chasis'],
                'numero_regrabacion_motor'=>$vehiculo['numero_regrabacion_motor'],
                'numero_regrabacion_serie'=>$vehiculo['numero_regrabacion_serie'],
                'numero_regrabacion_vin'=>$vehiculo['numero_regrabacion_vin'],
                'organismo_transito'=>$vehiculo['organismo_transito'],
                'identificador_pais_origen'=>$vehiculo['identificador_pais_origen'],
                'pais_origen'=>$vehiculo['pais_origen'],
                'peso_bruto_vehicular'=>$vehiculo['peso_bruto_vehicular'],
                'dias_matriculado'=>$vehiculo['dias_matriculado'],
                'es_regrabado_chasis'=>$vehiculo['es_regrabado_chasis'],
                'es_regrabado_motor'=>$vehiculo['es_regrabado_motor'],
                'es_regrabado_serie'=>$vehiculo['es_regrabado_serie'],
                'es_regrabado_vin'=>$vehiculo['es_regrabado_vin'],
                'fecha_matricula'=>$vehiculo['fecha_matricula'],
                'tiene_prendas'=>$vehiculo['tiene_prendas'],
                'tiene_gravámenes'=>$vehiculo['tiene_gravámenes'],
                'capacidad_de_pasajeros'=>$vehiculo['capacidad_de_pasajeros'],

            ];
        }
    } catch (\Exception $e) {
        $app->response()->status(400);
        $result = ['error' => $e->getMessage()];
    }

    $app->contentType('application/json');
    $app->response->setBody(json_encode($result));
})->name('listado-vehiculos'); 

$app->post('/proxy-runt/consulta/vehiculos', $requiresHttps, $authorization->withRequiredScope(['carSearch']), function () use ($app, $mongoDb) {
    

    //$connection = new Connection\Business();
    //$connection->holaMundo();

    //die;

    $placa = $app->request->post('placa');
    $vehiculo = $mongoDb->runt->findOne(array('numero_placa' => $placa));
        
    if ($vehiculo === null) {
        $app->response()->status(404);
        $vehiculo = ['error' => "Vehicle with placa '{$placa}' was not found"];
    }

    unset($vehiculo['_id']);
    $app->contentType('application/json');
    $app->response->setBody(json_encode($vehiculo));

})->name('detalle-vehiculo');


$app->run();
