#!/usr/bin/env php
<?php

$data = [
    [
       'identificador_linea'=>'1',
        'linea'=>'Prado sumo',
        'identificador_marca'=>'476',
        'marca'=>'TOYOTA',
        'modelo'=>'M1',
        'numero_de_chasis'=>'87898JHJ989HKL8',
        'numero_ejes'=>'0',
        'numero_licencia_transito'=>'121212',
        'numero_motor'=>'12121212JHFJH3212',
        'numero_placa'=>'NAT206',
        'numero_registro'=>'87897',
        'numero_serie'=>'121',
        'numero_vin'=>'212',
        'capacidad_carga'=>'0',
        'cilindraje'=>'2700',
        'identificador_clase_vehiculo'=>'545',
        'clase_vehiculo'=>'CAMPERO CAMIONETA',
        'clasificacion'=>'1',
        'identificador_color'=>'22',
        'color'=>'blanco',
        'estado_del_vehiculo'=>'',
        'numero_regrabacion_chasis'=>'',
        'numero_regrabacion_motor'=>'',
        'numero_regrabacion_serie'=>'',
        'numero_regrabacion_vin'=>'',
        'organismo_transito'=>'',
        'identificador_pais_origen'=>'',
        'pais_origen'=>'',
        'peso_bruto_vehicular'=>'',
        'dias_matriculado'=>'',
        'es_regrabado_chasis'=>'',
        'es_regrabado_motor'=>'',
        'es_regrabado_serie'=>'',
        'es_regrabado_vin'=>'',
        'fecha_matricula'=>'',
        'tiene_prendas'=>'N',
        'tiene_gravámenes'=>'N',
        'capacidad_de_pasajeros'=>''
    ],
    [
       'identificador_linea'=>'2',
        'linea'=>'M1',
        'identificador_marca'=>'678',
        'marca'=>'BMW',
        'modelo'=>'M1',
        'numero_de_chasis'=>'909089DSKJKJ98',
        'numero_ejes'=>'0',
        'numero_licencia_transito'=>'121212',
        'numero_motor'=>'879868655JNKIOU98',
        'numero_placa'=>'BRJ055',
        'numero_registro'=>'2232',
        'numero_serie'=>'989',
        'numero_vin'=>'767',
        'capacidad_carga'=>'0',
        'cilindraje'=>'2000',
        'identificador_clase_vehiculo'=>'989',
        'clase_vehiculo'=>'SEDAN',
        'clasificacion'=>'1',
        'identificador_color'=>'29',
        'color'=>'NEGRO',
        'estado_del_vehiculo'=>'',
        'numero_regrabacion_chasis'=>'',
        'numero_regrabacion_motor'=>'',
        'numero_regrabacion_serie'=>'',
        'numero_regrabacion_vin'=>'',
        'organismo_transito'=>'',
        'identificador_pais_origen'=>'',
        'pais_origen'=>'',
        'peso_bruto_vehicular'=>'',
        'dias_matriculado'=>'',
        'es_regrabado_chasis'=>'',
        'es_regrabado_motor'=>'',
        'es_regrabado_serie'=>'',
        'es_regrabado_vin'=>'',
        'fecha_matricula'=>'',
        'tiene_prendas'=>'N',
        'tiene_gravámenes'=>'N',
        'capacidad_de_pasajeros'=>''
    ],
    
];

$runt = (new MongoClient())->selectDb('slim_oauth2')->runt;
$runt->drop();
foreach ($data as $cars) {
    $runt->insert($cars);
}



print_r($runt);
